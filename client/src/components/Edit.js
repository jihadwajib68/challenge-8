import React, { Component } from 'react';
import Result from './Result';
import Swal from 'sweetalert2';

class Edit extends Component {
    constructor(props) {
      super(props);
      this.state = {
        username: '',
        email: '',
        password: '',
        result: '',
        data: ''
      };
      
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }
  
    handleChange(event) {
      const target = event.target;
      const value = target.value;
      const name = target.name;

      this.setState({
        [name]: value
      });
    }

    sendThru() {
      this.setState({
        'username': '',
        'email': '',
        'password': '',
      });
    }
  
    handleSubmit(e) {
      e.preventDefault();
      Swal.fire({
        titleText: 'Success edit data',
        icon: 'success',
      }).then((result) => {
        if(result.isConfirmed) {
          this.setState({ result: 'success' });
          this.setState({
            data: {
              username: this.state.username,
              email: this.state.email,
              password: this.state.password
            }
          })
          this.sendThru();
        }
      });
    }
  
    render() {
      return (
        <div className='d-flex flex-column justify-content-center align-items-center mt-5'>
          <h1 className='mb-4'>FORM EDIT PLAYER</h1>
          {this.state.result === '' && <div className='card w-50'>
            <div className='card-body'>
              <form onSubmit={this.handleSubmit}>
                <div className="form-floating mb-3">
                  <input type="text" className="form-control" name="username" placeholder="Username" value={this.state.username} onChange={this.handleChange} required/>
                  <label for="username" className="form-label">Username</label>
                </div>
                <div className="form-floating mb-3">
                  <input type="email" className="form-control" name="email" placeholder="Email" value={this.state.email} onChange={this.handleChange} required/>
                  <label for="email" className="form-label">Email</label>
                </div>
                <div className="form-floating mb-3">
                  <input type="password" className="form-control" name="password" placeholder="Password" value={this.state.password} onChange={this.handleChange} required/>
                  <label for="password" className="form-label">Password</label>
                </div>
                <input type="submit" value="Submit" className='btn btn-warning'/>
              </form>
            </div>
          </div>}
          {this.state.result === 'success' && <Result data={this.state.data} type="register" />}
        </div>
      );
    }
}

export default Edit;