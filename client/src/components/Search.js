import React, { Component } from 'react';
import Result from './Result';

function getSearch() {
  return new Promise((resolve) => {
    setTimeout(() => resolve([1, 2, 3]), 1000);
  });
}

class Search extends Component {
    constructor(props) {
      super(props);
      this.state = {
        isLoading: false,
        search: '',
        result: '',
        data: ''
      };
      
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }
  
    handleChange(event) {
      const target = event.target;
      const value = target.value;
      const name = target.name;

      this.setState({
        [name]: value
      });
    }

    sendThru() {
      this.setState({
        'search': '',
      });
    }
  
    handleSubmit(e) {
      e.preventDefault();
      this.setState({ isLoading: true });

      getSearch().then(() => {
        this.setState({
          isLoading: false,
          result: 'success',
          data: {
            search: this.state.search,
          }
        });
        this.sendThru();
      });
    }
  
    render() {
      return (
        <div className='d-flex flex-column justify-content-center align-items-center mt-5'>
          <h1 className='mb-4'>SEARCH PLAYER</h1>
          {this.state.result === '' && <div className='card w-50'>
            <div className='card-body'>
              <form onSubmit={this.handleSubmit}>
                <div className="form-floating mb-3">
                  <input type="text" className="form-control" name="search" placeholder="Search" value={this.state.search} onChange={this.handleChange} />
                  <label for="search" className="form-label"><i>Search by Username, Email, Experience, or Level</i></label>
                </div>
                {this.state.isLoading ? <div className="spinner-border text-secondary" role="status"><span className="visually-hidden">Loading...</span></div> : <button disabled={this.state.isLoading} className='btn btn-secondary'>Search</button>}
              </form>
            </div>
          </div>}
          {this.state.result === 'success' && <Result data={this.state.data} type="search" />}
        </div>
      );
    }
}

export default Search;