import React, { Component } from 'react';
import Register from './components/Register';
import Edit from './components/Edit';
import Search from './components/Search';
import './App.css';

class App extends Component {
  state = {
    showing: '',
  }

  handleRegister = () => {
    this.setState({ showing: 'register' })
  }

  handleEdit = () => {
    this.setState({ showing: 'edit' })
  }

  handleSearch = () => {
    this.setState({ showing: 'search' })
  }

  handleHome = () => {
    this.setState({ showing: '' })
  }

  render() {
    return (
      <div className="App">
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <div className="container-fluid">
            <a className="navbar-brand" href="/#">THE GAME</a>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
                <li className="nav-item">
                  <span className={!this.state.showing ? 'nav-link active' : 'nav-link'} role="button" onClick={this.handleHome}>Home</span>
                </li>
                <li className="nav-item">
                  <span className={this.state.showing === 'register' ? 'nav-link active' : 'nav-link'} role="button" onClick={this.handleRegister}>Register</span>
                </li>
                <li className="nav-item">
                  <span className={this.state.showing === 'edit' ? 'nav-link active' : 'nav-link'} role="button" onClick={this.handleEdit}>Edit</span>
                </li>
                <li className="nav-item">
                  <span className={this.state.showing === 'search' ? 'nav-link active' : 'nav-link'} role="button" onClick={this.handleSearch}>Search</span>
                </li>
              </ul>
            </div>
          </div>
        </nav>
        <div>
          {!this.state.showing && <h1>HOME</h1>}
          {this.state.showing === 'register' && <Register />}
          {this.state.showing === 'edit' && <Edit />}
          {this.state.showing === 'search' && <Search />}
        </div>
      </div>
    );
  }
}

export default App;
